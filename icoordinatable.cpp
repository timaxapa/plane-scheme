#include "icoordinatable.h"

ICoordinatable::ICoordinatable()
{
    coords = BoxCoordinates(1, 1, 1, 1);
}

ICoordinatable::ICoordinatable(BoxCoordinates _coords)
{
    coords = _coords;
}

BoxCoordinates ICoordinatable::getCoordinates()
{
    return this->coords;
}
