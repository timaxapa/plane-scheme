#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "room.h"
#include <QPushButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QSignalMapper>
#include <QMessageBox>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsProxyWidget>
#include <QSpacerItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Описываем структуру комнаты с ящиками (самолета со шкафами)
    Room room(QList<Case>()
              // Название шкафа и список корзин в нем
              << Case("Case 1", QList<Basket>()
                      // Название корзины и список плат в ней (потом этот список передадим на 3ю вкладку)
                      << Basket("Basket 1", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(1, 1, 1, 1))
                      << Basket("Basket 2", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Horizontal,
                                BoxCoordinates(1, 2, 1, 1))
                      << Basket("Basket 3", QList<Board>()
                                << Board("Board 1", BoxCoordinates(1, 1, 1, 1))
                                << Board("Board 2", BoxCoordinates(1, 2, 1, 1))
                                << Board("Board 3", BoxCoordinates(2, 1, 1, 1))
                                << Board("Board 4", BoxCoordinates(2, 2, 1, 1))
                                << Board("Board 5", BoxCoordinates(3, 1, 1, 1))
                                << Board("Board 6", BoxCoordinates(3, 2, 1, 1)),
                                BoxOrientation::Manual,
                                BoxCoordinates(2, 1, 1, 1))
                      << Basket("Basket 4", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(2, 2, 1, 1))
                      << Basket("Basket 5", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(3, 1, 2, 1))
                      , BoxCoordinates(1, 1, 1, 1))
              << Case("Case 2", QList<Basket>()
                      << Basket("Basket 1", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6")
                                << Board("Board 7")
                                << Board("Board 8")
                                << Board("Board 9")
                                << Board("Board 10")
                                << Board("Board 11")
                                << Board("Board 12")
                                << Board("Board 13")
                                << Board("Board 14")
                                << Board("Board 15"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(1, 1, 1, 1))
                      << Basket("Basket 2", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(2, 1, 1, 1))
                      << Basket("Basket 3", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(3, 1, 1, 1))
                      << Basket("Basket 4", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(4, 1, 1, 1))
                      << Basket("Basket 5", QList<Board>()
                                << Board("Board 1")
                                << Board("Board 2")
                                << Board("Board 3")
                                << Board("Board 4")
                                << Board("Board 5")
                                << Board("Board 6"),
                                BoxOrientation::Vertical,
                                BoxCoordinates(5, 1, 1, 1))
                      , BoxCoordinates(1, 2, 1, 1))
              );


    // Перебираем шкафы с оборудованием и вставляем шкаф по координатам на место
    for (int i = 0; i < room.cases.size(); ++i)
    {

        // Берем текущий шкаф
        Case c = room.cases.at(i);

        // Создаем групбокс, даем ему имя шкафа и в который положим потом кнопки
        QGroupBox *groupBox = new QGroupBox();
        groupBox->setTitle(c.name);

        // Сетка для расположения кнопок корзин
        QGridLayout *basketLayout = new QGridLayout(groupBox);

        for (int i = 0; i < c.baskets.size(); ++i)
        {
            // Берем корзину в шкафу
            Basket b = c.baskets.at(i);

            // Создаем для нее кнопку и пишем в нее название корзины
            QPushButton *button = new QPushButton();
            button->setText(b.getName());
            button->setCursor(Qt::CursorShape::PointingHandCursor);

            // Привязываем клик по кнопке к открытию третьей вкладки
            QSignalMapper* signalMapper = new QSignalMapper(this);
            connect (button, SIGNAL(clicked()), signalMapper, SLOT(map()));

            BasketWrapper *basketWrapper = new BasketWrapper(b, this);

            signalMapper->setMapping(button, basketWrapper);
            connect(signalMapper, SIGNAL(mapped(QObject*)), this, SLOT(OpenBasket(QObject*)));

            // Пихаем кнопку в наш layout и идем дальше
            BoxCoordinates coords = b.getCoordinates();
            basketLayout->addWidget(button, coords.x, coords.y, coords.rowSpan, coords.colSpan);
        }

        // Вставляем шкаф по координатам на место. Если что, берем еще соседнее место
        BoxCoordinates coords = c.getCoordinates();
        ui->roomGridLayout->addWidget(groupBox, coords.x, coords.y, coords.rowSpan, coords.colSpan);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Клик на корзину
void MainWindow::OpenBasket(QObject *object)
{
    BasketWrapper *b = (BasketWrapper*) object;
    this->selectedBasket = b->basket;
    ui->tabWidget->setCurrentIndex(2);
}

// Метод вызывается, когда меняется вкладка
void MainWindow::on_tabWidget_currentChanged(int index)
{
    // Если мы перешли на 3ю вкладку (индексы с 0)
    if (index == 2)
    {
        Basket basket = this->selectedBasket;
        QList<Board> boards = basket.getBoards();

        QGroupBox *groupBox = new QGroupBox();
        groupBox->setTitle(basket.getName());

        // Смотрим на заданную для Корзины ориентацию и рисуем платы
        // Вертикально - один столбик
        if (basket.getOrientation() == BoxOrientation::Vertical || basket.getOrientation() == BoxOrientation::Horizontal)
        {
            QVBoxLayout *layout = new QVBoxLayout(groupBox);

            QWidget *widget = new QWidget();
            widget->setMinimumWidth(400);
            widget->setPalette(QPalette(Qt::white));

            QVBoxLayout *innerLayout = new QVBoxLayout(widget);

            for (int i = 0; i < boards.size(); ++i)
            {
                Board board = boards.at(i);

                // Создаем кнопку и пишем в нее название платы
                QPushButton *button = new QPushButton();
                button->setText(board.getName());
                button->setCursor(Qt::CursorShape::PointingHandCursor);

                // Привязываем клик по кнопке к открытию платы
                QSignalMapper* signalMapper = new QSignalMapper(this);
                connect (button, SIGNAL(clicked()), signalMapper, SLOT(map()));

                BoardWrapper *boardWrapper = new BoardWrapper(board, this);

                signalMapper->setMapping(button, boardWrapper);
                connect(signalMapper, SIGNAL(mapped(QObject*)), this, SLOT(OpenBoard(QObject*)));

                innerLayout->addWidget(button);
            }

            if (basket.getOrientation() == BoxOrientation::Horizontal)
            {
                // Если ориентация горизонтальная - переворачиваем widget с layout
                QGraphicsView *graphicsView = new QGraphicsView();
                graphicsView->setStyleSheet("QGraphicsView { border-style: none; }");
                QGraphicsScene *scene = new QGraphicsScene();
                QGraphicsProxyWidget *w = scene->addWidget(widget);
                w->setRotation(-90);
                graphicsView->setScene(scene);

                layout->addWidget(graphicsView);
            }
            else
            {
                layout->addWidget(widget);
            }
        }

        // Если ориентация Manual - расставляем все по сетке
        if (basket.getOrientation() == BoxOrientation::Manual)
        {
            QGridLayout *layout = new QGridLayout(groupBox);

            for (int i = 0; i < boards.size(); ++i)
            {
                Board board = boards.at(i);

                // Создаем кнопку и пишем в нее название платы
                QPushButton *button = new QPushButton();
                button->setText(board.getName());
                button->setCursor(Qt::CursorShape::PointingHandCursor);

                // Привязываем клик по кнопке к открытию платы
                QSignalMapper* signalMapper = new QSignalMapper(this);
                connect (button, SIGNAL(clicked()), signalMapper, SLOT(map()));

                BoardWrapper *boardWrapper = new BoardWrapper(board, this);

                signalMapper->setMapping(button, boardWrapper);
                connect(signalMapper, SIGNAL(mapped(QObject*)), this, SLOT(OpenBoard(QObject*)));

                BoxCoordinates coords = board.getCoordinates();
                layout->addWidget(button, coords.x, coords.y, coords.rowSpan, coords.colSpan);
            }
        }

        ui->basketLayout->addWidget(groupBox);
    }
    else
    {
        QLayoutItem *child;
        while ((child = ui->basketLayout->layout()->takeAt(0)) != NULL)
        {
            delete child->widget();
            delete child;
        }
    }
}

// Клик на плату
void MainWindow::OpenBoard(QObject *object)
{
    BoardWrapper *boardWrapper = (BoardWrapper*) object;
    QMessageBox msgBox;
    msgBox.setText("Clicked on " + boardWrapper->board.getName());
    msgBox.exec();
}
