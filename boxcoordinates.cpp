#include "boxcoordinates.h"


BoxCoordinates::BoxCoordinates()
{
}


BoxCoordinates::BoxCoordinates(int _x, int _y)
{
    BoxCoordinates(_x, _y, 1, 1);
}

BoxCoordinates::BoxCoordinates(int _x, int _y, int _colSpan, int _rowSpan)
{
    x = _x;
    y = _y;
    colSpan = _colSpan;
    rowSpan = _rowSpan;
}
