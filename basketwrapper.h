#ifndef BASKETHOLDER_H
#define BASKETHOLDER_H

#include <QObject>
#include "basket.h"

class BasketWrapper : public QObject
{
    Q_OBJECT
public:
    explicit BasketWrapper(Basket basket, QObject *parent = 0);
    Basket basket;
signals:

public slots:
};

#endif // BASKETHOLDER_H
