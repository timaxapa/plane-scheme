#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "basket.h"
#include "basketwrapper.h"
#include "boardwrapper.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void OpenBasket(QObject *object);
    void OpenBoard(QObject *object);
    void on_tabWidget_currentChanged(int index);

private:
    Ui::MainWindow *ui;
    Basket selectedBasket;
};

#endif // MAINWINDOW_H
