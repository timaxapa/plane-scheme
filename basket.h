#ifndef BASKET_H
#define BASKET_H
#include <QString>
#include <QList>
#include "board.h"
#include "boxcoordinates.h"
#include "icoordinatable.h"
#include "boxorientation.h"

// Корзина
class Basket : public ICoordinatable
{
public:
    Basket();
    Basket(QString name, QList<Board> boards, BoxOrientation orientation, BoxCoordinates coords);

    QString getName();
    QList<Board> getBoards();
    BoxOrientation getOrientation();
private:
    QString name;
    QList<Board> boards;
    BoxOrientation orientation;
};

#endif // BASKET_H
