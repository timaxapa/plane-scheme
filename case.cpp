#include "case.h"

Case::Case(QString _name, QList<Basket> _baskets, BoxCoordinates _coords) : ICoordinatable(_coords)
{
    name = _name;
    baskets = _baskets;
}
