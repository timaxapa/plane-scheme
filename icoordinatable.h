#ifndef ICOORDINATABLE_H
#define ICOORDINATABLE_H
#include "boxcoordinates.h"


class ICoordinatable
{
public:
    ICoordinatable();
    ICoordinatable(BoxCoordinates coords);

    BoxCoordinates getCoordinates();
private:
    BoxCoordinates coords;
};

#endif // ICOORDINATABLE_H
