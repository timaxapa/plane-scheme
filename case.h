#ifndef CASE_H
#define CASE_H
#include <QString>
#include <QList>
#include "basket.h"
#include "boxcoordinates.h"
#include "icoordinatable.h"

// Шкаф
class Case : public ICoordinatable
{
public:
    Case(QString name, QList<Basket> baskets, BoxCoordinates coords);
    QString name;
    QList<Basket> baskets;
};

#endif // CASE_H
