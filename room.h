#ifndef ROOM_H
#define ROOM_H
#include <QList>
#include "case.h"


class Room
{
public:
    Room(QList<Case>);
    QList<Case> cases;
};

#endif // ROOM_H
