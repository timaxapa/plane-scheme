#ifndef BOARDWRAPPER_H
#define BOARDWRAPPER_H

#include <QObject>
#include "board.h"

class BoardWrapper : public QObject
{
    Q_OBJECT
public:
    explicit BoardWrapper(Board board, QObject *parent = 0);
    Board board;

signals:

public slots:
};

#endif // BOARDWRAPPER_H
