#ifndef BOXCOORDINATES_H
#define BOXCOORDINATES_H


class BoxCoordinates
{
public:
    BoxCoordinates();
    BoxCoordinates(int x, int y);
    BoxCoordinates(int x, int y, int colSpan, int rowSpan);

    int x;
    int y;
    int colSpan;
    int rowSpan;
};

#endif // BOXCOORDINATES_H
