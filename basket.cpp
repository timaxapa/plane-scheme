#include "basket.h"

Basket::Basket() : ICoordinatable()
{

}

Basket::Basket(QString _name, QList<Board> _boards, BoxOrientation _orientation, BoxCoordinates _coords) : ICoordinatable(_coords)
{
    name = _name;
    boards = _boards;
    orientation = _orientation;
}

QString Basket::getName()
{
    return name;
}

QList<Board> Basket::getBoards()
{
    return boards;
}


BoxOrientation Basket::getOrientation()
{
    return orientation;
}
