#ifndef BOARD_H
#define BOARD_H
#include <QString>
#include "icoordinatable.h"

// Плата
class Board : public ICoordinatable
{
public:
    Board();
    Board(QString name);
    Board(QString name, BoxCoordinates coords);

    QString getName();
private:
    QString name;
};

#endif // BOARD_H
