#include "board.h"

Board::Board()
{
}

Board::Board(QString _name)
{
    name = _name;
}

Board::Board(QString _name, BoxCoordinates _coords) : ICoordinatable(_coords)
{
    name = _name;
}

QString Board::getName()
{
    return name;
}
