#ifndef POSITIONTYPE_H
#define POSITIONTYPE_H

enum BoxOrientation
{
    Vertical,
    Horizontal,
    Manual
};

#endif // POSITIONTYPE_H
